public class Week2Loops {

        public static void main(String[] args) {
            String text = "Pam parapam";
            int textLength = text.length();
            if (textLength % 2 == 0)
                System.out.println("The string is even.");
            else
                System.out.println("The string is odd.");
        }
    }
