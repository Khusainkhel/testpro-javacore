public class Palindrome
{
    public static void main(String args[])
    {
        String original = "BorrowOrRob",
                originalToLower = original.toLowerCase(),
                reverse = "";
        int length = original.length();

        for (int i = length - 1; i >= 0; i--)
            reverse = reverse + originalToLower.charAt(i);

        if (originalToLower.equals(reverse)){
            System.out.println("The string is a palindrome.");
        }
        else {
            System.out.println("The string isn't a palindrome.");

        }

    }
}